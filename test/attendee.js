/* eslint-disable */

const mongoose = require('mongoose');
const chai = require('chai');
const chaiHttp = require('chai-http');
const Attendee = require('../models/attendee');
const server = require('../server');
const should = chai.should();
const expect = chai.expect;

const testAttendee = {
  'name': 'Ekonash',
  'email': 'ashinzekene@gmail.com',
  'company': 'ashinzekene'
};

chai.use(chaiHttp);
const chaiReq = chai.request(server).keepOpen();

describe('ATTENDEES', () => {
  beforeEach((done) => {
    Attendee.deleteMany({}, err => {
      done()
    });
  });

  it('GET /attendees/ Should get attendees', async () => {
    const res = await chaiReq.get('/attendees')
    res.should.have.status(200);
    res.body.should.be.an('array');
  });

  it('POST /attendees/ Should create an attendee', async () => {
    const res = await chaiReq.post('/attendees').send(testAttendee)
    expect(res.status).to.equal(200);
    expect(res.body).to.be.an('object').with.property('name');
  })

  it('POST /attendees/ Should not create attendee wtihout name', async () => {
    const newAttendee = Object.assign({}, testAttendee);
    delete newAttendee.name;
    const res = await chaiReq.post('/attendees').send(newAttendee)
    expect(res.status).to.equal(400);
    expect(res.body).to.be.an('object').with.property('err', 'Could not create attendee');
  })

  it('GET /attendees/:attendee Should get an attendee already created', async () => {
    const res = await chaiReq.post('/attendees').send(testAttendee)
    expect(res.status).to.equal(200);
    expect(res.body).to.be.an('object').with.property('name');

    const res2 = await chaiReq.get(`/attendees/${res.body._id}`);
    expect(res2.status).to.equal(200);
    expect(res2.body).to.be.an('object').with.property('name');
  })

  it('DELETE /attendees/:attendee Should get remove an already exisiting attendee', async () => {
    const res = await chaiReq.post('/attendees').send(testAttendee)
    expect(res.status).to.equal(200);
    expect(res.body).to.be.an('object').with.property('name');

    const res2 = await chaiReq.delete(`/attendees/${res.body._id}`)
    expect(res2.status).to.equal(200);
    expect(res2.body).to.be.an('object').with.property('name');
  })
})