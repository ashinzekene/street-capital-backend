/* eslint-disable */

const mongoose = require('mongoose');
const chai = require('chai');
const chaiHttp = require('chai-http');
const Talk = require('../models/talk');
const Attendee = require('../models/attendee');
const server = require('../server');
const expect = chai.expect;

const testAttendee = {
  'name': 'Ekonash',
  'email': 'ashinzekene@gmail.com',
  'company': 'ashinzekene'
};

const testTalk = {
  'title': 'Reactivity in JavaScript',
  'abstract': 'combining proxies, to do watch properties and for computed values ',
  'room': 'A304',
  'speaker': {
    'name': 'Ekene Ashinze',
    'email': 'ashinzekene@gmail.com',
    'company': 'ashinzekene'
  }
};

chai.use(chaiHttp);
const chaiReq = chai.request(server).keepOpen();

describe('TALKS', () => {
  beforeEach(done => {
    Talk.deleteMany({}, err => {
      Attendee.deleteMany({}, err => {
        done();
      });
    });
  });

  it('GET /talks Should get talks', async () => {
    const res = await chaiReq.get('/talks');
    expect(res).to.have.status(200);
    expect(res.body).to.be.an('array');
  });

  it('POST /talks Should create a talk', async () => {
    const talk = await chaiReq.post('/talks').send(testTalk);
    expect(talk.status).to.equal(200);
    expect(talk.body).to.be.an('object').with.property('title');
  })

  it('POST /talks Should not create a talk without a speaker', async () => {
    const newTalk = Object.assign({}, testTalk);
    delete newTalk.speaker;
    const talk = await chaiReq.post('/talks').send(newTalk);
    expect(talk.status).to.equal(400);
    expect(talk.body).to.be.an('object').with.property('err', 'Could not add talk');
  })

  it('GET /talks/:id Should get a talk already created', async () => {
    const res = await chaiReq.post('/talks').send(testTalk);
    expect(res.status).to.equal(200);
    expect(res.body).to.be.an('object').with.property('title', 'Reactivity in JavaScript');
    expect(res.body).to.be.an('object').with.property('room', 'A304');

    const res2 = await chaiReq.get(`/talks/${res.body._id}`);
    expect(res2.status).to.equal(200);
    expect(res2.body).to.be.an('object').with.property('title', 'Reactivity in JavaScript');
    expect(res2.body).to.be.an('object').with.property('room', 'A304');
  })

  it('DELETE /talks/:id Should get remove an already exisiting talk', async () => {
    const res = await chaiReq.post('/talks').send(testTalk);
    expect(res.status).to.equal(200);
    expect(res.body).to.be.an('object').with.property('title');

    const res2 = await chaiReq.delete(`/talks/${res.body._id}`)
    expect(res2.status).to.equal(200);
    expect(res2.body).to.be.an('object').with.property('title', 'Reactivity in JavaScript');
    expect(res2.body.speaker).to.be.an('object').to.include({
      name: 'Ekene Ashinze',
      company: "ashinzekene"
    });;
  })

  it('DELETE /talks/:id Should fail if no talk exists', async () => {
    const res = await chaiReq.del(`/talks/a8b83848944949499}`);
    expect(res.status).to.equal(400);
    expect(res.body).to.be.an('object').with.property('err', 'Could not remove talk');
  })

  it('/POST /:talk/attendee/:attendee Should add attendee', async () => {
    const talk = await chaiReq.post('/talks').send(testTalk);
    const attendee = await chaiReq.post('/attendees').send(testAttendee);
    expect(talk.status).to.equal(200);
    expect(attendee.status).to.equal(200);

    const talk2 = await chaiReq.post(`/talks/${talk.body._id}/attendee/${attendee.body._id}`);
    expect(talk2.body.attendees.length).to.equal(1);
    expect(talk2.body.attendees).to.include(attendee.body._id);
  })

  it('/DELETE /:talk/attendee/:attendee Should remove attendee', async () => {
    const talk = await chaiReq.post('/talks').send(testTalk);
    const attendee = await chaiReq.post('/attendees').send(testAttendee);
    expect(talk.status).to.equal(200);
    expect(attendee.status).to.equal(200);

    const talk2 = await chaiReq.post(`/talks/${talk.body._id}/attendee/${attendee.body._id}`);
    expect(talk2.body.attendees.length).to.equal(1);
    expect(talk2.body.attendees).to.include(attendee.body._id);

    const talk3 = await chaiReq.del(`/talks/${talk.body._id}/attendee/${attendee.body._id}`);
    expect(talk3.body.attendees.length).to.equal(0);
  })
})