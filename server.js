require('dotenv').config();
const express = require('express');
const logger = require('morgan');
const mongoose = require('mongoose');

mongoose.connect(process.env.MONGO_URL, { useNewUrlParser: true }, (err) => {
  if (err) return process.stdout.emit(err, '\n');
  process.stdout.emit(`mongoose started on URL:${process.env.MONGO_URL}`);
  return 0;
});

const attendeeRoute = require('./routes/attendee');
const talkRoute = require('./routes/talk');

const app = express();

app.use(logger('tiny'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.get('/', (req, res) => {
  res.send(`
  <body style="background-color: #111; color: #ccc">
    <h2>Welcome to my street capital test</h2>
    <p> Available routes:</p>
    <code>
    GET /attendees - Get all attendees</br>
    POST /attendees - Create an attendee</br>
    GET /attendees/:id - Get an attendee by id</br>
    DELETE /attendees/:id - Delete an attendee by id</br>
    PATCH /attendees/:id - Edit an attendee</br>
    </br>
    </br>
    GET /talks - Get all talks</br>
    POST /talks - Create a talk</br>
    GET /talks/:id - Get a talk by id</br>
    DELETE /talks/:id - Delete a talk by id</br>
    POST /talks/:talk/attendee/:attendee - Add an attendee to a talk</br>
    DELETE /talks/:talk/attendee/:attendee - Remove an attendee from a talk</br>
    </code>
  </body>
  `);
});
app.use('/attendees', attendeeRoute);
app.use('/talks', talkRoute);
// eslint-disable-next-line
app.use((err, req, res, next) => {
  console.log('Error', err.message);
  res.status(500).json({
    msg: 'A server error has occurred',
    err: err.message,
  });
});

module.exports = app;
