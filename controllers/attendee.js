const Attendee = require('../models/attendee');
const { handleError } = require('../utils');

module.exports = function _() {
  return {
    add(req, res) {
      const { name, company, email } = req.body;
      const attendee = new Attendee();
      attendee.name = name;
      attendee.company = company;
      attendee.email = email;
      attendee.registered = (new Date()).toISOString();
      attendee.save((err) => {
        if (err) {
          handleError(res, err, 'Could not create attendee');
          return;
        }
        res.json(attendee);
      });
    },
    getAll(req, res) {
      Attendee.find({}, (err, attendees) => {
        if (err) {
          handleError(res, err, 'Could not get all attendees');
          return;
        }
        res.json(attendees);
      });
    },
    getById(req, res) {
      Attendee.findById(req.params.id, (err, attendee) => {
        if (err) {
          handleError(res, err, 'Could not get attendee');
          return;
        }
        res.json(attendee);
      });
    },
    remove(req, res) {
      Attendee.findByIdAndRemove(req.params.id, (err, attendee) => {
        if (err) {
          handleError(res, err, 'Could not remove attendee');
          return;
        }
        res.json(attendee);
      });
    },
    update(req, res) {
      Attendee.findByIdAndUpdate(req.body, (err, attendee) => {
        if (err) {
          handleError(res, err, 'Could not update attendee');
          return;
        }
        res.json(attendee);
      });
    },
  };
};
