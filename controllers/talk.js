const Talk = require('../models/talk');
const { handleError } = require('../utils');

module.exports = function _() {
  return {
    add(req, res) {
      const {
        title, abstract, room, speaker,
      } = req.body;
      const talk = new Talk();
      talk.title = title;
      talk.abstract = abstract;
      talk.room = room;
      talk.speaker = speaker;
      talk.registered = (new Date()).toISOString();
      talk.save((err) => {
        if (err) {
          handleError(res, err, 'Could not add talk');
          return;
        }
        res.json(talk);
      });
    },
    getAll(req, res) {
      Talk.find({}, (err, talks) => {
        if (err) {
          handleError(res, err, 'Could not get all talks');
          return;
        }
        res.json(talks);
      });
    },
    getById(req, res) {
      Talk.findById(req.params.id, (err, talk) => {
        if (err) {
          handleError(res, err, 'Could not get talk');
          return;
        }
        res.json(talk);
      });
    },
    remove(req, res) {
      Talk.findByIdAndRemove(req.params.id, (err, talk) => {
        if (err) {
          handleError(res, err, 'Could not remove talk');
          return;
        }
        res.json(talk);
      });
    },
    update(req, res) {
      Talk.findByIdAndUpdate(req.body, (err, talk) => {
        if (err) {
          handleError(res, err, 'Could not update talk');
          return;
        }
        res.json(talk);
      });
    },
    addAttendee(req, res) {
      Talk.findByIdAndUpdate(req.params.talk,
        { $addToSet: { attendees: res.attendee._id } }, // eslint-disable-line no-underscore-dangle
        { new: true }, (err, talk) => {
          if (err) {
            handleError(res, err, 'Could not add attendee to talk');
            return;
          }
          res.json(talk);
        });
    },
    removeAttendee(req, res) {
      Talk.findByIdAndUpdate(req.params.talk,
        { $pull: { attendees: res.attendee._id } }, // eslint-disable-line no-underscore-dangle
        { new: true }, (err, talk) => {
          if (err) {
            handleError(res, err, 'could not remove atendee');
            return;
          }
          res.json(talk);
        });
    },
  };
};
