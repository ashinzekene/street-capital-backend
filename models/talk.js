const mongoose = require('mongoose');
const attendeeSchema = require('./attendee').schema;

const { Schema } = mongoose;

const talkSchema = new Schema({
  title: {
    type: String,
    required: true,
  },
  abstract: {
    type: String,
    required: true,
  },
  room: {
    type: String,
    required: true,
  },
  speaker: {
    type: attendeeSchema,
    required: true,
  },
  attendees: [{
    type: Schema.Types.ObjectId,
    ref: 'Attendee',
  }],
}, { timestamps: true });

const Talk = mongoose.model('Talk', talkSchema);

module.exports = Talk;
