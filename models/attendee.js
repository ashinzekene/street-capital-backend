const mongoose = require('mongoose');

const { Schema } = mongoose;

const attendeeSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  company: {
    type: String,
  },
  email: {
    type: String,
    required: true,
    unique: true,
  },
  bio: String,
  registered: {
    type: String,
  },
});

const Attendee = mongoose.model('Attendee', attendeeSchema);

module.exports = Attendee;
