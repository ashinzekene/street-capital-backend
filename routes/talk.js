const express = require('express');
const talk = require('../controllers/talk')();
const { extractAttendee } = require('../utils');

const router = express.Router();

// Get all talks
router.get('/', talk.getAll);

// Create a talk
router.post('/', talk.add);

// Get a talk
router.get('/:id', talk.getById);

// Delete a talk
router.delete('/:id', talk.remove);

// Delete a talk
router.delete('/:id', talk.update);

// Add an attendee to a talk
router.post('/:talk/attendee/:attendee', extractAttendee, talk.addAttendee);

// Remove an attendee from a talk
router.delete('/:talk/attendee/:attendee', extractAttendee, talk.removeAttendee);

module.exports = router;
