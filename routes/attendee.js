const express = require('express');
const attendee = require('../controllers/attendee')();

const router = express.Router();

// Get all attendes
router.get('/', attendee.getAll);

// Create an attendee
router.post('/', attendee.add);

// Get an attendee
router.get('/:id', attendee.getById);

// Delete an attendee
router.delete('/:id', attendee.remove);

// Update an attendee
router.patch('/:id', attendee.update);

module.exports = router;
