const Attendee = require('./models/attendee');

module.exports.extractAttendee = (req, res, next) => {
  Attendee.findById(req.params.attendee, (err, attendee) => {
    if (err) {
      res.status(400).json({ err: 'An error occurred while fetching attendee' });
      return;
    }
    res.attendee = attendee;
    next();
  });
};

module.exports.handleError = (res, err, msg = 'something pathetic happened. Sorry') => {
  res.status(400).json({
    err: msg,
    msg: err.message || 'An unknown error occurred',
  });
};
